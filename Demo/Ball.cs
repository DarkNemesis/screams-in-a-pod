﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;

public class Ball : Node
{
	public static bool mStatus = false;
	public static float mSpeed = 9f;

	private HashSet<People> mCapturedPeople = new HashSet<People>();
    AudioManager audioManager;

    //private float mFuelValue;
	private SpriteFont mFont;

	private float mFontScale;

    public bool mBeingAbsorbed = false;
    public bool hasRyan = false;
    private UFO mUFO;
   

	Animation mAnimation;

	public Ball(Texture2D texture, SpriteFont font, Vector2 position, float angle)
	{
		Type = NodeType.PodEmpty;
		mFontScale = 1f;

	    mSprite = new Sprite(texture);
		mFont = font;
        
        audioManager = new AudioManager();
        
        mSprite.TextureRectangle = new Rectangle(0, 0, 50, 50);
        mSprite.Origin = new Vector2(0.5f, 0.5f);
        mSprite.Position = new Vector2(position.X, position.Y);
        audioManager.mLoadContent();

		mAnimation = new Animation(mSprite, new Rectangle(0, 100, 50, 50), 5, new TimeSpan(0, 0, 0, 0, 100));

        mVelocity = new Vector2(mSpeed * (float)Math.Sin(angle), mSpeed * (float)Math.Cos(angle));
	}

    public override void resolveCollision(Node collider)
    {

        if (collider.Type == NodeType.Ryan)
            hasRyan = true;

        if (collider.Type == NodeType.LivingBeing || collider.Type == NodeType.Ryan)
		{
           
			mCapturedPeople.Add((People)collider);
			if (mType == NodeType.PodEmpty)
			{
				mType = NodeType.PodFilled;
				mAnimation = new Animation(mSprite, new Rectangle(0, 0, 50, 50), 5, new TimeSpan(0, 0, 0, 0, 50));
				mVelocity *= 2;				
			}
		}

        if (collider.Type == NodeType.UFO && mVelocity.Y < 0 && mBeingAbsorbed == false)
        {
			foreach (People people in mCapturedPeople)
				people.ToBeRemoved = true;

            mUFO = (UFO)collider;
            mBeingAbsorbed = true;
            mStatus = false;
        }

    }

    public int getNumberOfPeople()
    {
        return mCapturedPeople.Count;
    }

    public void setBallSpeed(float newSpeed)
    {
        mSpeed = newSpeed;
    }

    public float getFuelValue()
     {
		float fuel = 0;
		foreach (People people in mCapturedPeople)
		{
			fuel += people.Fuel;
		}
		return fuel;
	 }
  
	public override void Update(GameTime gameTime)
	{
        Rectangle bounds = mSprite.getBoundingBox();
        if (bounds.X + bounds.Width > Demo.GameWorld.mRightBuilding && mVelocity.X > 0 || bounds.X < Demo.GameWorld.mLeftBuilding && mVelocity.X < 0)
        { mVelocity.X *= -1; audioManager.playSound(2); }
        if (bounds.Y + bounds.Height > Demo.GameWorld.mGround && mVelocity.Y > 0)
        { mVelocity.Y *= -1; audioManager.playSound(2); }
		if (bounds.Y + bounds.Height < 0 && mVelocity.Y < 0)
		{
			foreach (People people in mCapturedPeople)
				people.ToBeRemoved = true;

			mToBeRemoved = true;
			mStatus = false;
		}
        if (mBeingAbsorbed)
        {
            if ((mSprite.Position - mUFO.getUFOCenter()).Length() <= 8)
            {
                mToBeRemoved = true;
            }
            else
            {
                mSprite.Scale = Math.Max(0.1f, mSprite.Scale - 0.02f);
				mFontScale = Math.Max(0.1f, mFontScale - 0.02f);
				Vector2 direction = mUFO.getUFOCenter() - mSprite.Position;
				direction.Normalize();                
                mVelocity = (7 * direction);
            }
        }
		mSprite.Position += mVelocity;
		mAnimation.Update(gameTime);
	}
	public override void Draw(SpriteBatch spriteBatch)
	{
		mSprite.Draw(spriteBatch);
		if (mCapturedPeople.Count > 0)
		{
			Vector2 size = mFont.MeasureString(mCapturedPeople.Count.ToString());
			spriteBatch.DrawString(mFont, mCapturedPeople.Count.ToString(), mSprite.Position, Color.Black, 0, size / 2, mFontScale, SpriteEffects.None, 0);
		}
	}
}
