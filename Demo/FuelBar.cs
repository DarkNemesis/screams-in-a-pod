﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

public class FuelBar
{
	UFO mUFO;
	Sprite mFuelBar;
	Sprite mFuel;

	public FuelBar(Texture2D fuelBar, Texture2D fuel, UFO ufo)
	{
		mUFO = ufo;
		mFuelBar = new Sprite(fuelBar);
		mFuelBar.Position = new Vector2(1500, 50);

		mFuel = new Sprite(fuel);
		mFuel.Position = new Vector2(1556, 79);
	}

	virtual public void Draw(SpriteBatch spriteBatch)
	{
		mFuel.TextureRectangle = new Rectangle(0, 0, (int)(166 * mUFO.getFuelValue() / 100.0f), 26);
		mFuelBar.Draw(spriteBatch);
		mFuel.Draw(spriteBatch);
	}
}