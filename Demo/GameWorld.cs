﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Media;
using System;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;

namespace Demo
{
    public class GameWorld : Game
    {
		GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        AudioManager audioManager;
        ContentManager contentManager;
		List<Node> mGameUnits = new List<Node>();
		UFO mUFO;
		FuelBar mFuelBar;
        public enum GameState
        {
            startMenu, playingGame, pauseScreen, endScreen
        };

        GameState currentState;
		public const int mLeftBuilding = 90;
		public const int mRightBuilding = 1830;
		public const int mGround = 1000;

        private Texture2D mGameBackground;
        private Texture2D mStartScreen;
        private List<Texture2D> mEndScreen;
        private Song mGameMusic;
        private Song mMenuMusic;
        private Song mRyanMusic;
        private SpriteFont fuelFont;
        private SpriteFont endFont;
		private String mGameOverString;
        private bool isItRyanTime = false;
        private bool ryanTimeTriggered = false;
        private bool musicChange = false;

        private List<SoundEffect> audio;

		private TimeSpan mTotalGameTime;
        private int endScreenIndex;

		//Player Spawn Mechanics
		TimeSpan mSpawnTimer;
		TimeSpan mSpawnCooldown;
        TimeSpan mRyanModeStart;

		public GameWorld()
        {
            graphics = new GraphicsDeviceManager(this);

			graphics.PreferredBackBufferWidth = 1920;  // set this value to the desired width of your window
			graphics.PreferredBackBufferHeight = 1080;   // set this value to the desired height of your window
#if DEBUG
#else
			graphics.IsFullScreen = true;
#endif
			graphics.ApplyChanges();
            currentState = GameState.startMenu;
            audioManager = new AudioManager();
            audio = new List<SoundEffect>();
            mEndScreen = new List<Texture2D>();
			Content.RootDirectory = "Content";
            contentManager = new ContentManager(Content.ServiceProvider);
            endScreenIndex = 0;
        }
		  
        protected override void Initialize()
        {
			mSpawnTimer = new TimeSpan(166667 * 50);
			mSpawnCooldown = new TimeSpan(166667 * 50);

			mGameUnits.Add(new UFO(Content));
			mUFO = (UFO)mGameUnits[0];

			mFuelBar = new FuelBar(Content.Load<Texture2D>("FuelBar"), Content.Load<Texture2D>("Fuel"), mUFO);

			mGameOverString = "";

			base.Initialize();
		}

        protected override void LoadContent()
        {            
            spriteBatch = new SpriteBatch(GraphicsDevice);

            audioManager.mLoadContent();

//#if DEBUG
//#else
			mGameMusic = Content.Load<Song>("ost");
            mMenuMusic = Content.Load<Song>("menu");
            mRyanMusic = Content.Load<Song>("ryanmode");
            MediaPlayer.Play(mMenuMusic);
            MediaPlayer.IsRepeating = true;
            
           
//#endif
			mGameBackground = Content.Load<Texture2D> ("background");
            mStartScreen = Content.Load<Texture2D> ("enter");
            mEndScreen.Add(Content.Load<Texture2D>("score board-1"));
            mEndScreen.Add(Content.Load<Texture2D>("score board-2"));
            mEndScreen.Add(Content.Load<Texture2D>("score board-4"));
            mEndScreen.Add(Content.Load<Texture2D>("score board-5"));
            mEndScreen.Add(Content.Load<Texture2D>("score board-6"));
            audio.Add(Content.Load<SoundEffect>("basicmanscream"));

            endFont = Content.Load<SpriteFont>("endGameScreen");
            fuelFont = Content.Load<SpriteFont>("font");
        }

        protected override void UnloadContent()
        {}

		protected void shoot()
		{	
			if (Ball.mStatus == false)
			{
				mGameUnits.Add(new Ball(Content.Load<Texture2D>("Ball"), Content.Load<SpriteFont>("font"), mUFO.getGunPosition(), mUFO.getGunAngle() * -1));
				Ball.mStatus = true;
			}			
		}

        protected override void Update(GameTime gameTime)
        {
           	switch(currentState)
            {
                case GameState.startMenu:
                  
                    
                    updateStartMenu(gameTime);
                    break;
                case GameState.playingGame:
                  
                    
                    updateGame(gameTime);
                    break;
                case GameState.pauseScreen:
                    updatePauseMenu(gameTime);
                    break;
                case GameState.endScreen:
                   
                    updateEndScreen(gameTime);
                    break;

            }
			base.Update(gameTime);
        }

        void updateGame(GameTime gameTime)
        {
             if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();


			if ((int)mTotalGameTime.TotalSeconds == 50 && ryanTimeTriggered == false)
			{
				isItRyanTime = true;
			}

			if (isItRyanTime && ryanTimeTriggered == false)
			{
				bool flag = true;
				foreach (Node node in mGameUnits)
				{
					if (node.Type == Node.NodeType.LivingBeing)
					{
						flag = false;
						((People)node).Laps = 1;
					}
				}
				if (flag)
					triggerRyanTime();
			}
			
            if (mUFO.isGameOver())
			{
                currentState=GameState.endScreen;
                MediaPlayer.Stop();
                MediaPlayer.Play(mMenuMusic);
                MediaPlayer.IsRepeating = true;
				mGameOverString = "You survived for " + (int)mTotalGameTime.TotalSeconds + " seconds and captured " + mUFO.getPeopleAbductedCount() + " people\n";
			}
			else
			{
				if (Keyboard.GetState().IsKeyDown(Keys.Right))
					mUFO.moveUFO(UFO.UFODirection.Right);
				if (Keyboard.GetState().IsKeyDown(Keys.Left))
					mUFO.moveUFO(UFO.UFODirection.Left);

				if (Keyboard.GetState().IsKeyDown(Keys.A))
					mUFO.moveGun(UFO.GunDirection.Right);
				if (Keyboard.GetState().IsKeyDown(Keys.D))
					mUFO.moveGun(UFO.GunDirection.Left);

                if (Keyboard.GetState().IsKeyDown(Keys.Space))
                { if(Ball.mStatus == false) audioManager.playSound(1); shoot();  }
                if(Keyboard.GetState().IsKeyDown(Keys.P))
                    mUFO.changeFuel(100);

				for (int i = 0; i < mGameUnits.Count; i++)
					mGameUnits[i].Update(gameTime);
				
				resolveCollision();


                if (!isItRyanTime)
                {
                    spawnPlayer(gameTime);
                }
                if( mUFO.RyanModeActive && !musicChange)
                {
                    MediaPlayer.Stop();
                    MediaPlayer.Play(mRyanMusic);
                    musicChange = true;
                    mRyanModeStart = gameTime.TotalGameTime;
                    Ball.mSpeed = 12f;
                }

                if (mUFO.RyanModeActive && isItRyanTime)
                {                    
                    spawnRyan(gameTime);
                }
				mTotalGameTime = gameTime.TotalGameTime;
			}
        }

        public void triggerRyanTime()
        {            
            mGameUnits.Add(new Ryan(Content.Load<Texture2D>("Ryan"), Content.Load<Texture2D>("RyanDead")));
            ryanTimeTriggered = true;
        }


            void updateStartMenu(GameTime gameTime)
        {
            
             if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Enter))
            { MediaPlayer.Stop(); MediaPlayer.Play(mGameMusic); MediaPlayer.IsRepeating = true; currentState = GameState.playingGame; }
        }

        void updatePauseMenu(GameTime gameTime)
        {
            
        }

        void updateEndScreen(GameTime gameTime)
        {
             if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Enter))
                Exit();
        }

		public void spawnPlayer(GameTime gameTime)
		{
			if (mSpawnTimer >= mSpawnCooldown)
			{
				Random random = new Random();
				int r = random.Next(0, 100);

				if (r <= 9)
					mGameUnits.Add(new Cow(Content.Load<Texture2D>("Cow"), Content.Load<Texture2D>("CowDead")));
				else if (r <= 19)
					mGameUnits.Add(new Kid(Content.Load<Texture2D>("Kid"), Content.Load<Texture2D>("KidDead")));
				else
					mGameUnits.Add(new Man(Content.Load<Texture2D>("Man"), Content.Load<Texture2D>("ManDead")));

				mSpawnTimer = mSpawnTimer.Subtract(mSpawnCooldown);
			}
			mSpawnTimer = mSpawnTimer.Add(gameTime.ElapsedGameTime);
		}

        public void spawnRyan(GameTime gameTime)
        {
            if (mSpawnTimer >= mSpawnCooldown)
            {              
                mGameUnits.Add(new Dew(Content.Load<Texture2D>("Dew"), Content.Load<Texture2D>("Dew")));

                mSpawnTimer = mSpawnTimer.Subtract(mSpawnCooldown);
            }
            mSpawnTimer = mSpawnTimer.Add(gameTime.ElapsedGameTime);            

            if ((int)(mTotalGameTime.TotalSeconds) - (int)mRyanModeStart.TotalSeconds == 15)
            {
				foreach (Node node in mGameUnits)
				{
					if (node.Type == Node.NodeType.LivingBeing)
					{						
						node.ToBeRemoved = true;
					}
				}

				isItRyanTime = false;
                MediaPlayer.Stop();
                MediaPlayer.Play(mGameMusic);
                mUFO.turnToNormalMode();
                Ball.mSpeed = 9f;
            }
        }

		public void resolveCollision()
		{
			HashSet<Node> toRemove = new HashSet<Node>();

			for (int i = 0; i < mGameUnits.Count; i++)
			{
				for (int j = i + 1; j < mGameUnits.Count; j++)
				{
					if (mGameUnits[i].getBoundingBox().Intersects(mGameUnits[j].getBoundingBox()))
					{
						mGameUnits[i].resolveCollision(mGameUnits[j]);
						mGameUnits[j].resolveCollision(mGameUnits[i]);
					}
				}
			}

			for (int i = mGameUnits.Count - 1; i >= 0; i--)
			{
				if (mGameUnits[i].ToBeRemoved)
					mGameUnits.RemoveAt(i);
			}
		}

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.White);

			switch(currentState)
            {
                case GameState.startMenu:
                    drawStartMenu(gameTime);
                    break;
                case GameState.playingGame:
                    drawGameplay(gameTime);
                    break;
                case GameState.pauseScreen:
                    drawPauseMenu(gameTime);
                    break;
                case GameState.endScreen:
                    drawEndScreen(gameTime);
                    break;

            }

			base.Draw(gameTime);
        }

        void drawStartMenu(GameTime gameTime)
        {
            spriteBatch.Begin();
            spriteBatch.Draw(mStartScreen, new Vector2(0,0),Color.White);
            spriteBatch.End();
        }

        void drawGameplay(GameTime gameTime)
        {
            spriteBatch.Begin();

            spriteBatch.Draw(mGameBackground, new Vector2(0, 0), Color.White);

            spriteBatch.DrawString(fuelFont, "People Consumed : " + mUFO.getPeopleAbductedCount(), new Vector2(100, 50), Color.Red);
            spriteBatch.DrawString(fuelFont,"Time:" + (int)mTotalGameTime.TotalSeconds, new Vector2(1400,10),Color.LimeGreen);

            if(ryanTimeTriggered && !mUFO.RyanModeActive)
                spriteBatch.DrawString(fuelFont, "CATCH RYAN", new Vector2(800, 500), Color.LimeGreen);
            if (isItRyanTime && mUFO.RyanModeActive)
                spriteBatch.DrawString(fuelFont, "RYAN MODE\n DO THE DEW", new Vector2(100, 100), Color.LimeGreen);

            for (int i = 0; i < mGameUnits.Count; i++)
				mGameUnits[i].Draw(spriteBatch);
			
			spriteBatch.DrawString(fuelFont, mGameOverString, new Vector2(100, 1080/2), Color.White, 0, new Vector2(0,0), 1, SpriteEffects.None, 0);
			mFuelBar.Draw(spriteBatch);

			spriteBatch.End();
        }
        void drawPauseMenu(GameTime gameTime)
        {

        }
        void drawEndScreen(GameTime gameTime)
        {
            if (mUFO.getPeopleAbductedCount() < 10)
                endScreenIndex = 0;
            else if (mUFO.getPeopleAbductedCount() > 10 && mUFO.getPeopleAbductedCount() < 25)
                endScreenIndex = 1;
            else if(mUFO.getPeopleAbductedCount() > 24 && mUFO.getPeopleAbductedCount() < 40)
                endScreenIndex = 2;
            else if(mUFO.getPeopleAbductedCount() > 39 && mUFO.getPeopleAbductedCount() < 60)
                endScreenIndex = 3;
            else
                endScreenIndex = 4;
            spriteBatch.Begin();
            spriteBatch.Draw(mEndScreen[endScreenIndex], new Vector2(0,0),Color.White);
            spriteBatch.DrawString(endFont, mGameOverString, new Vector2(300,200), Color.White);
            spriteBatch.End();
        }
    }
}
