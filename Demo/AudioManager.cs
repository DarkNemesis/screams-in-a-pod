﻿

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

using System.Collections.Generic;

using Microsoft.Xna.Framework.Audio;

public class AudioManager : Game
{
    public List<SoundEffect> soundEffects;
    GraphicsDeviceManager graphics;
    SoundEffectInstance soundEffectInstance;


    public AudioManager()
    {
        soundEffects = new List<SoundEffect>();
        graphics = new GraphicsDeviceManager(this);
        Content.RootDirectory = "Content";

       
    }
   public void mLoadContent()
    {
        soundEffects.Add(Content.Load<SoundEffect>("basicmanscream"));
        soundEffects.Add(Content.Load<SoundEffect>("newshoot"));
        soundEffects.Add(Content.Load<SoundEffect>("podbounce"));
        soundEffects.Add(Content.Load<SoundEffect>("cowmoo"));
        soundEffects.Add(Content.Load<SoundEffect>("littleboyscream"));
        soundEffects.Add(Content.Load<SoundEffect>("dewslurp"));


    }

    public void playSound(int soundIndex)
    {
        if(soundIndex == 1)
        {
            soundEffectInstance = soundEffects[soundIndex].CreateInstance();
            soundEffectInstance.Volume = 0.4f;
            soundEffectInstance.Play();
        }
        else
            soundEffects[soundIndex].Play();
    }
}
