﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

public class Node
{
	public enum NodeType { UFO, PodEmpty, PodFilled, LivingBeing, Ryan };

	protected Sprite mSprite;

	protected Vector2 mVelocity;
	public Vector2 Velocity
	{
		get { return mVelocity; }
		set { mVelocity = value; }
	}

	protected NodeType mType;
	public NodeType Type
	{
		get { return mType; }
		protected set { mType = value; }
	}

	protected bool mToBeRemoved = false;
	public bool ToBeRemoved
	{
		get { return mToBeRemoved; }
		set { mToBeRemoved = value; }
	}

	public Rectangle getBoundingBox()
	{
		return mSprite.getBoundingBox();
	}

	virtual public void Draw(SpriteBatch spriteBatch)
	{
        mSprite.Draw(spriteBatch);
	}

	virtual public void resolveCollision(Node collider)
	{}

	virtual public void Update(GameTime gameTime)
	{}
}
