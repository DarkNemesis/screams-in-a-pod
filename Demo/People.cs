﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

public class People : Node
{
	protected int mMaxLaps;
	public int Laps
	{
		set { mMaxLaps = value; }
	}

	protected int mCurrentLap = 0;
	protected float mFuel;
	public float Fuel
	{
		get { return mFuel; }
		set { mFuel = value; }
	}
    AudioManager audioManager;
	protected Animation mAnimation;
	protected Texture2D mCapturedTexture;
	protected Ball mBall = null;
    protected int soundIndex;

	public People()
	{}
	public People(Texture2D texture, Texture2D capturedTexture)
	{
		mCapturedTexture = capturedTexture;
		mSprite = new Sprite(texture);
        audioManager = new AudioManager();
	}

	public void init(int speed)
	{
		

		int direction = new Random().Next(0, 2);

		if (direction == 0)
		{
			mSprite.Position = new Vector2(Demo.GameWorld.mLeftBuilding + 1, Demo.GameWorld.mGround - mAnimation.FrameRect.Height);
			mVelocity = new Vector2(speed, 0);
		}
		else
		{
			mSprite.Position = new Vector2(Demo.GameWorld.mRightBuilding - mAnimation.FrameRect.Width - 1, Demo.GameWorld.mGround - mAnimation.FrameRect.Height);
			mVelocity = new Vector2(-speed, 0);
			mSprite.reverse();
		}			
	}

	public override void Update(GameTime gameTime)
	{
		if (mBall == null)
		{
			Rectangle bounds = mSprite.getBoundingBox();
			if (bounds.X + bounds.Width >= Demo.GameWorld.mRightBuilding && mVelocity.X > 0 || bounds.X < Demo.GameWorld.mLeftBuilding && mVelocity.X < 0)
			{
				mVelocity.X *= -1;
				mSprite.reverse();
				mCurrentLap++;
				if (mCurrentLap >= mMaxLaps)
					mToBeRemoved = true;
			}

			mAnimation.Update(gameTime);
			mSprite.Position += mVelocity;
		}
		else
		{
			mSprite.Position = new Vector2(mBall.getBoundingBox().Left + mVelocity.X, mBall.getBoundingBox().Bottom);
		}        
	}

	public override void resolveCollision(Node collider)
	{
		if (mBall == null && (collider.Type == NodeType.PodEmpty || collider.Type == NodeType.PodFilled))
		{
			mBall = (Ball)collider;
			mSprite = new Sprite(mCapturedTexture);
			Random random = new Random();
           
            audioManager.mLoadContent();
            audioManager.playSound(soundIndex);
                       
			int x = random.Next(0, 51);
			mSprite.Position = new Vector2(mBall.getBoundingBox().Left + x, mBall.getBoundingBox().Bottom);
			mSprite.Origin = new Vector2(0.5f, 0f);
			mVelocity = new Vector2(x, 0f);
		}			
	}
}

public class Man : People
{
	public Man(Texture2D texture, Texture2D capturedTexture) : base(texture, capturedTexture)
	{		
		mFuel = 0.5f;
        soundIndex = 0;
        mType = NodeType.LivingBeing;

        Random random = new Random();
		int speed = random.Next(2, 10);
		switch (speed)
		{
			case 2:
			case 3: mMaxLaps = 1; break;
			case 4:
			case 5: mMaxLaps = random.Next(1, 3); break;
			case 6:
			case 7: mMaxLaps = random.Next(1, 4); break;
			case 8:
			case 9: mMaxLaps = random.Next(2, 5); break;
		};				
		
		mAnimation = new Animation(mSprite, new Rectangle(0, 0, 135, 235), 30, new TimeSpan(0, 0, 0, 0, 2500 / Math.Abs(speed)));
		init(speed);	
	}
}

public class Cow : People
{
	public Cow(Texture2D texture, Texture2D capturedTexture) : base(texture, capturedTexture)
	{
		mFuel = 10;
		mMaxLaps = 1;
        soundIndex = 3;
        mType = NodeType.LivingBeing;

        int speed = new Random().Next(8, 12);
		
		mAnimation = new Animation(mSprite, new Rectangle(0, 0, 246, 160), 30, new TimeSpan(50000000 / Math.Abs(speed)));
		init(speed);
	}
}

public class Kid : People
{
	public Kid(Texture2D texture, Texture2D capturedTexture) : base(texture, capturedTexture)
	{
		mFuel = -5;
		mMaxLaps = 1;
        soundIndex = 4;
        mType = NodeType.LivingBeing;

        int speed = new Random().Next(5, 10);

		mAnimation = new Animation(mSprite, new Rectangle(0, 0, 97, 124), 32, new TimeSpan(0, 0, 0, 0, 3500 / Math.Abs(speed)));
		init(speed);
	}
}

public class Ryan : People
{
	public Ryan(Texture2D texture, Texture2D capturedTexture) : base(texture, capturedTexture)
	{
		mFuel = 0;
		mMaxLaps = 15;
        mType = NodeType.Ryan;
		soundIndex = 4;

		int speed = 5;

		mAnimation = new Animation(mSprite, new Rectangle(0, 0, 135, 235), 30, new TimeSpan(0, 0, 0, 0, 1000));
		init(speed);
	}
}

public class Dew : People
{
	public Dew(Texture2D texture, Texture2D capturedTexture) : base(texture, capturedTexture)
	{
		mFuel = 5;
		mMaxLaps = 1;
        mType = NodeType.LivingBeing;

        soundIndex = 5;

		int speed = 5;
		mSprite.Rotation = 0.3f;

		mAnimation = new Animation(mSprite, new Rectangle(0, 0, 73, 253), 1, new TimeSpan(0, 0, 0, 0, 1000));
		init(speed);
	}
}