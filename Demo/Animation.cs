﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

public class Animation
{
	protected Sprite mSprite;
	public Sprite Sprite
	{
		get { return mSprite; }
		set { mSprite = value; }
	}

	Rectangle mFrameRect;
	public Rectangle FrameRect
	{
		get { return mFrameRect; }
		set { mFrameRect = value; }
	}

	int mFrames;
	public int Frames
	{
		get { return mFrames; }
		set { mFrames = value; }
	}

	int mCurrentFrame;
	public int CurrentFrame
	{
		get { return mCurrentFrame; }
		set { mCurrentFrame = value; }
	}

	TimeSpan mDuration;
	public TimeSpan Duration
	{
		get { return mDuration; }
		set { mDuration = value; }
	}

	TimeSpan mElapsedTime;
	public TimeSpan ElapsedTime
	{
		get { return mElapsedTime; }
		set { mElapsedTime = value; }
	}
	
	public Animation()
	{}

	public Animation(Sprite sprite, Rectangle frameRect, int frames, TimeSpan duration)
	{
		mSprite = sprite;
		mSprite.TextureRectangle = frameRect;
		mFrameRect = frameRect;
		mFrames = frames;
		mDuration = duration;
	}

	public void Update(GameTime gameTime)
	{
		TimeSpan timePerFrame = new TimeSpan(mDuration.Ticks / mFrames);

		Vector2 textureBounds = new Vector2(mSprite.Texture.Bounds.Width, mSprite.Texture.Bounds.Height);
		Rectangle textureRect = mSprite.TextureRectangle;

		if (mCurrentFrame == 0)
			textureRect = mFrameRect;
				
		for (mElapsedTime += gameTime.ElapsedGameTime; mElapsedTime >= timePerFrame; mElapsedTime -= timePerFrame)
		{			
			textureRect.X += textureRect.Width;
			
			if (textureRect.X + textureRect.Width > textureBounds.X)
			{				
				textureRect.X = 0;
				textureRect.Y += textureRect.Height;
			}

			mCurrentFrame = (mCurrentFrame + 1) % mFrames;
			if (mCurrentFrame == 0)
				textureRect = mFrameRect;
		}		
		mSprite.TextureRectangle = textureRect;
	}
}