﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using System;

public class UFO : Node
{
	const float	mGunAngleBounds = 0.6f;
	const float mUFOAngleBounds = 0.4f;

	ContentManager mContentManager;

	Animation mAnimation;

    Sprite mGun;

	float mUFORotatingDirection;

	int mPeopleAbducted;

	//Fuel Mechanics for the UFO
	float mFuel;

	const float mMileagePerFuel = 9999;
	float mMileageSinceLastFuel;
    public bool RyanModeActive = false;

	TimeSpan mHoverPerFuel = new TimeSpan(0, 0, 0, 0, 500);
	TimeSpan mHoverSinceLastFuel;

	public UFO(ContentManager Content)
	{
		Type = NodeType.UFO;

		mContentManager = Content;

		Texture2D gunTexture = mContentManager.Load<Texture2D>("Gun");

		mSprite = new Sprite(mContentManager.Load<Texture2D>("UFO"));
		turnToNormalMode();
		mSprite.Position = new Vector2(1920 / 2, 120);

		mGun = new Sprite(gunTexture);
        mGun.Rotation = 0.0f;
        mGun.Origin = new Vector2(0.5f, 0.0f);
        mGun.Position = new Vector2(540, 190);

        mVelocity = new Vector2(10, 0);

        mFuel = 100;        
		mUFORotatingDirection = 0.008f;

		mHoverSinceLastFuel = new TimeSpan();

		mPeopleAbducted = 0;        
	}

	public void turnToRyanMode()
	{
		Vector2 bounds = mSprite.Position;
		mSprite = new Sprite(mContentManager.Load<Texture2D>("RyanUFO"));
		mAnimation = new Animation(mSprite, new Rectangle(0, 0, 373, 192), 5, new TimeSpan(0, 0, 0, 0, 100));
		mSprite.Origin = new Vector2(0.5f, 0.5f);
		mSprite.Rotation = 0.0f;
		mSprite.Position = new Vector2(bounds.X, bounds.Y);
		mVelocity = new Vector2(20, 0);

        RyanModeActive = true;

	}

	public void turnToNormalMode()
	{
		Vector2 bounds = mSprite.Position;
		mSprite = new Sprite(mContentManager.Load<Texture2D>("UFO"));
		mAnimation = new Animation(mSprite, new Rectangle(0, 0, 373, 192), 5, new TimeSpan(0, 0, 0, 0, 100));
		mSprite.Origin = new Vector2(0.5f, 0.5f);
		mSprite.Rotation = 0.0f;
		mSprite.Position = new Vector2(bounds.X, bounds.Y);
		mVelocity = new Vector2(10, 0);
	}

	public bool isGameOver()
	{
		return (mFuel <= 0);		
	}

	public enum UFODirection {Left, Right};
	public void moveUFO(UFODirection dir)
	{
		if (dir == UFODirection.Left)
            mSprite.Position -= mVelocity;
		else
            mSprite.Position += mVelocity;

		mMileageSinceLastFuel += mVelocity.X;
		if (mMileageSinceLastFuel >= mMileagePerFuel)
		{
			mMileageSinceLastFuel -= mMileagePerFuel;
			mFuel--;
		}
	}

	public enum GunDirection { Left, Right };
	public void moveGun(GunDirection dir)
	{
		if (dir == GunDirection.Left)
			mGun.Rotation -= 0.05f;
		else
			mGun.Rotation += 0.05f;
	}

	public int getPeopleAbductedCount()
	{
		return mPeopleAbducted;
	}

	public override void resolveCollision(Node collider)
	{
        if (collider.Type == NodeType.PodFilled && ((Ball)collider).mBeingAbsorbed == false)
		{
             if (((Ball)collider).hasRyan)
                turnToRyanMode();
            mFuel = Math.Min(100, mFuel + ((Ball)collider).getFuelValue());
			mPeopleAbducted+=((Ball)collider).getNumberOfPeople();
		}
	}

	public Vector2 getGunPosition()
	{
		return new Vector2(mGun.Position.X, mGun.Position.Y + mGun.getBoundingBox().Height);
	}

    public Vector2 getUFOCenter()
    {
		Rectangle bounds = mSprite.getBoundingBox();
        return new Vector2(bounds.X + bounds.Width / 2, bounds.Y + bounds.Height / 2);
    }
	public float getGunAngle()
	{
		return mGun.Rotation + mSprite.Rotation;
	}

    public float getFuelValue()
    {
        return mFuel;
    }

    public void changeFuel(int changeValue)
    {
        mFuel -= changeValue;
    }

	public override void Update(GameTime gameTime)
	{
        float leftCheck = Math.Max(mSprite.Position.X, Demo.GameWorld.mLeftBuilding + mSprite.getBoundingBox().Width / 2);
        float rightCheck = Math.Min(leftCheck, Demo.GameWorld.mRightBuilding - mSprite.getBoundingBox().Width / 2);
        mSprite.Position = new Vector2(rightCheck, mSprite.Position.Y);
        
        mGun.Rotation = Math.Max(mGun.Rotation, -mGunAngleBounds);
        mGun.Rotation = Math.Min(mGun.Rotation, mGunAngleBounds);       
		
		mHoverSinceLastFuel = mHoverSinceLastFuel.Add(gameTime.ElapsedGameTime);

        mSprite.Rotation += mUFORotatingDirection;

		if ((mSprite.Rotation >= 0.3f && mUFORotatingDirection >= 0) || (mSprite.Rotation <= -0.3f && mUFORotatingDirection <= 0))
			mUFORotatingDirection *= -1;

		mSprite.Position = new Vector2(mSprite.Position.X, mSprite.Position.Y + 3 * (float)Math.Sin(mSprite.Rotation));
        mGun.Position = new Vector2(mSprite.Position.X + 2* (float)Math.Sin(mSprite.Rotation), mGun.Position.Y + 3 * (float)Math.Sin(mSprite.Rotation));

        if (mHoverSinceLastFuel >= mHoverPerFuel)
		{
			mFuel--;
			mHoverSinceLastFuel = mHoverSinceLastFuel.Subtract(mHoverPerFuel);
		}

		mAnimation.Update(gameTime);
	}

	public override void Draw(SpriteBatch spriteBatch)
	{
        spriteBatch.Draw(mGun.Texture, mGun.Position, mGun.TextureRectangle, Color.White, mGun.Rotation + mSprite.Rotation, mGun.Origin, mGun.Scale, SpriteEffects.None, 0f);
		base.Draw(spriteBatch);
	}
}
