using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

public class Sprite
{
    protected Texture2D mTexture;
    public Texture2D Texture
    {
        get { return mTexture; }
        set { mTexture = value; }
    }

	protected Vector2 mPosition = new Vector2(0, 0);
    public Vector2 Position
    {
        get { return mPosition; }
        set { mPosition = value; }
    }

	protected Vector2 mOrigin = new Vector2(0, 0);
    public Vector2 Origin
    {
        get { return mOrigin; }
        set { mOrigin = new Vector2(mTextureRectangle.Width * value.X, mTextureRectangle.Height * value.Y);   }
    }

	protected float mScale = 1;
    public float Scale
    {
        get { return mScale; }
        set { mScale = value; }
    }

	protected float mRotation = 0;
    public float Rotation
    {
        get { return mRotation; }
        set { mRotation = value; }
    }

    protected Rectangle mTextureRectangle;
    public Rectangle TextureRectangle
    {
        get { return mTextureRectangle; }
        set { mTextureRectangle = value; }
    }

	protected SpriteEffects mSpriteEffect = SpriteEffects.None;
    Matrix mTransformMatrix;

    public Sprite(Texture2D texture)
    {
        mTexture = texture;
        mTextureRectangle = new Rectangle(0, 0, texture.Bounds.Width, texture.Bounds.Height);
    }

    public Sprite(Texture2D texture, Rectangle rect)
    {
        mTexture = texture;
        mTextureRectangle = rect;
    }

	public void reverse()
	{
		if (mSpriteEffect == SpriteEffects.None)
			mSpriteEffect = SpriteEffects.FlipHorizontally;
		else
			mSpriteEffect = SpriteEffects.None;
	}
    virtual public void Draw(SpriteBatch spriteBatch)
    {
        spriteBatch.Draw(mTexture, mPosition, mTextureRectangle, Color.White, mRotation, mOrigin, mScale, mSpriteEffect, 0f);
    }

    public Rectangle getBoundingBox()
    {
        mTransformMatrix = Matrix.CreateTranslation(new Vector3(-mOrigin, 0));
        mTransformMatrix *= Matrix.CreateScale(mScale);
        mTransformMatrix *= Matrix.CreateRotationZ(mRotation);
        mTransformMatrix *= Matrix.CreateTranslation(new Vector3(mPosition, 0));

        Vector2 topLeft     = Vector2.Transform(new Vector2(0, 0),              mTransformMatrix);
        Vector2 topRight    = Vector2.Transform(new Vector2(mTextureRectangle.Width, 0), mTransformMatrix);
        Vector2 bottomLeft  = Vector2.Transform(new Vector2(0, mTextureRectangle.Height), mTransformMatrix);
        Vector2 bottomRight = Vector2.Transform(new Vector2(mTextureRectangle.Width, mTextureRectangle.Height), mTransformMatrix);

        int minX = (int)Math.Min(topLeft.X, Math.Min(topRight.X, Math.Min(bottomLeft.X, bottomRight.X)));
        int maxX = (int)Math.Max(topLeft.X, Math.Max(topRight.X, Math.Max(bottomLeft.X, bottomRight.X)));
        int minY = (int)Math.Min(topLeft.Y, Math.Min(topRight.Y, Math.Min(bottomLeft.Y, bottomRight.Y)));
        int maxY = (int)Math.Max(topLeft.Y, Math.Max(topRight.Y, Math.Max(bottomLeft.Y, bottomRight.Y)));

        return new Rectangle(minX, minY, maxX - minX, maxY - minY);
    }
};